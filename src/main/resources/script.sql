create table IF NOT EXISTS character
(
    id        bigint not null
        primary key ,
    alias     varchar(255),
    full_name varchar(255),
    gender    varchar(255),
    picture   varchar(255)
);



create table IF NOT EXISTS franchise
(
    id          bigint not null
        primary key,
    description varchar(255),
    name        varchar(255)
);



create table IF NOT EXISTS movie
(
    id           bigint not null
        primary key,
    director     varchar(255),
    genre        varchar(255),
    picture      varchar(255),
    release_year varchar(255),
    title        varchar(255),
    trailer      varchar(255),
    franchise_id bigint
);



-- auto-generated definition
create sequence IF NOT EXISTS movie_sequence
start 1;
create sequence IF NOT EXISTS character_sequence start 1;
create sequence IF NOT EXISTS franchise_sequence start 1;
create sequence IF NOT EXISTS characters_movies_sequence start 1;
/*
 Franchises
 */

INSERT INTO franchise (id, name, description)
VALUES (1, 'f1', 'franchise 1');
INSERT INTO franchise (id, name, description)
VALUES (2, 'f2', 'franchise 2');
INSERT INTO franchise (id, name, description)
VALUES (3, 'f3', 'franchise 3');
/*
 Movies
 */
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (1, 'movie 1', 'genre 1', '2020', 'director 1', 'picture 1','trailer 1', 1);
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (2, 'movie 2', 'genre 2', '2021', 'director 2', 'picture 2','trailer 2', 2);
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (3, 'movie 3', 'genre 3', '2022', 'director 3',  'picture 3','trailer 3', 2);
/*
 Characters
 */
INSERT INTO character (id, full_name, alias, gender, picture)
VALUES (1, 'fullName 1', 'alias 1', 'gender 1', 'picture 1');
INSERT INTO character (id, full_name, alias, gender, picture)
VALUES (2, 'fullName 2', 'alias 2', 'gender 2', 'picture 2');
INSERT INTO character (id, full_name, alias, gender, picture)
VALUES (3, 'fullName 3', 'alias 3', 'gender 3', 'picture 3');


/*
 Link characters to movies
 */

/*
 Movie 1
 */
insert into characters_movies (id,character_id, movie_id) values(nextval('hibernate_sequence'),1,1);

/*
 Movie 2
 */
insert into characters_movies (id,character_id, movie_id) values(nextval('hibernate_sequence'),1,2);
insert into characters_movies (id,character_id, movie_id) values(nextval('hibernate_sequence'),2,2);


