package no.qux.demo.hibernate.controllers;


import no.qux.demo.hibernate.models.CommonResponse;
import no.qux.demo.hibernate.models.Franchise;
import no.qux.demo.hibernate.repositories.CharacterRepository;
import no.qux.demo.hibernate.repositories.FranchiseRepository;
import no.qux.demo.hibernate.repositories.MovieRepository;
import no.qux.demo.hibernate.services.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class FranchiseController {
    final
    FranchiseRepository franchiseRepository;
    final
    MovieRepository movieRepository;
    final
    CharacterRepository characterRepository;
    final
    MovieService movieService;
    public FranchiseController(FranchiseRepository franchiseRepository, MovieRepository movieRepository, CharacterRepository characterRepository, MovieService movieService) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
        this.movieService = movieService;
    }

    // Create

    @PostMapping("/franchises")
    public ResponseEntity<CommonResponse> createFranchise(@RequestBody Franchise f) {
        return ResponseEntity.ok(new CommonResponse(franchiseRepository.save(f)));
    }

    // Read
    @GetMapping("/franchises")
    public ResponseEntity<CommonResponse> getFranchises() {
        return ResponseEntity
                .ok()
                .body(new CommonResponse(franchiseRepository.findAll()));
    }

    @GetMapping("/franchises/{id}")
    public ResponseEntity<CommonResponse> getFranchiseById(@PathVariable Long id){
        if(franchiseRepository.findById(id).isPresent()){
            return ResponseEntity
                    .ok(new CommonResponse(franchiseRepository.findById(id).get()));
        }
        else return ResponseEntity.notFound().build();
    }

    @GetMapping("/franchises/{id}/movies")
    public ResponseEntity<CommonResponse> getMoviesByFranchiseId(@PathVariable Long id){
        if(franchiseRepository.findById(id).isPresent()){
            Franchise f = franchiseRepository.findById(id).get();
            var movies = movieService.getMoviesFromFranchise(f);
            var movieLinks = movies.stream().map(movie -> "/api/v1/movies/"+movie.getId());
            return ResponseEntity
                    .ok(new CommonResponse(movieLinks));
        }
        else return ResponseEntity.notFound().build();
    }

    @GetMapping("/franchises/{id}/characters")
    public ResponseEntity<CommonResponse> getCharactersByFranchiseId(@PathVariable Long id){
        if(franchiseRepository.findById(id).isPresent()){
            var franchise = franchiseRepository.findById(id).get();
            var characters = movieService.getCharactersFromFranchise(franchise);
            var characterLinks =  characters.stream().map(character -> "/api/v1/characters/"+character.getId());
            return ResponseEntity
                    .ok(new CommonResponse(characterLinks));
        }
        else return ResponseEntity.notFound().build();
    }

    // Update
    @PutMapping("/franchises/{id}")
    public ResponseEntity<CommonResponse> replaceFranchise(@PathVariable Long id, @RequestBody Franchise c) {
        if (franchiseRepository.findById(id).isPresent()) {
            return ResponseEntity.ok(new CommonResponse(franchiseRepository.save(c)));
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");

    }

    // Update
    @PutMapping("/franchises/{id}/movies")
    public ResponseEntity<CommonResponse> replaceFranchiseMovies(@PathVariable Long id, @RequestBody Long[] movieIds) {
        if (franchiseRepository.findById(id).isPresent()) {
            var franchise = franchiseRepository.findById(id).get();
            var movies = movieService.updateMoviesInFranchise(movieIds, franchise);
            var movieLinks = movies.stream().map(movie -> "/api/v1/movies/"+movie.getId());
            return ResponseEntity.ok(new CommonResponse(movieLinks));
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }

    // Delete
    @DeleteMapping("/franchises/{id}")
    public ResponseEntity<CommonResponse> deleteById(@PathVariable Long id) {
        var optional = franchiseRepository.findById(id);
        if (optional.isPresent()) {
            return ResponseEntity.ok(new CommonResponse(movieService.deleteFranchise(id)));
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }






}
