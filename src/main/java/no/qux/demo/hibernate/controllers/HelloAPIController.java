package no.qux.demo.hibernate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.hibernate.models.CommonResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Hello")
@RequestMapping("/hello")
public class HelloAPIController {
    @GetMapping("/")
    @Operation(summary = "Say Hello!")
    public ResponseEntity<CommonResponse> index() {
        return ResponseEntity
                .ok()
                .body(new CommonResponse("Hello, World!"));
    }
}
