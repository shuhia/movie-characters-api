package no.qux.demo.hibernate.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import no.qux.demo.hibernate.models.CommonResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Root")
public class RootAPIController {
    @GetMapping("/")
    public ResponseEntity<CommonResponse> getApiInfo() {
        return ResponseEntity
                .ok()
                .body(new CommonResponse(-1, "No API info yet"));
    }
}
