package no.qux.demo.hibernate.controllers;

import no.qux.demo.hibernate.models.Character;
import no.qux.demo.hibernate.models.CommonResponse;
import no.qux.demo.hibernate.repositories.CharacterRepository;
import no.qux.demo.hibernate.repositories.MovieRepository;
import no.qux.demo.hibernate.services.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class CharacterController {
    // Repository
    final
    CharacterRepository characterRepository;
    final
    MovieService movieService;

    public CharacterController(CharacterRepository characterRepository, MovieService movieService) {
        this.characterRepository = characterRepository;
        this.movieService = movieService;
    }

    // Create
    @PostMapping("/characters")
    public ResponseEntity<CommonResponse> createCharacter(@RequestBody Character c){
        return ResponseEntity
                .ok(new CommonResponse(characterRepository.save(c)));
    }
    // Read
    @GetMapping("/characters")
    public ResponseEntity<CommonResponse> getCharacters(){

        return ResponseEntity
                .ok(new CommonResponse(characterRepository.findAll()));
    }
    @GetMapping("/characters/{id}")
    public ResponseEntity<CommonResponse> getCharacterById(@PathVariable Long id){
        if(characterRepository.findById(id).isPresent()){
            return ResponseEntity
                    .ok(new CommonResponse(characterRepository.findById(id).get()));
        }
        else return ResponseEntity.notFound().build();
    }

    // Update
    @PutMapping("/characters/{id}")
    public ResponseEntity<CommonResponse> replaceCharacter(@PathVariable Long id, @RequestBody Character c){
        if(characterRepository.findById(id).isPresent()){
            return ResponseEntity
                    .ok(new CommonResponse(characterRepository.save(c)));
        }
        else return ResponseEntity.notFound().build();
    }
    // Delete
    @DeleteMapping("/characters/{id}")
    public ResponseEntity<CommonResponse> deleteById(@PathVariable Long id){
        var optional = characterRepository.findById(id);
            if(optional.isPresent()) {
                return ResponseEntity
                        .ok(new CommonResponse(movieService.deleteCharacter(id)));
            }
            else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }



}
