package no.qux.demo.hibernate.controllers;


import no.qux.demo.hibernate.models.CommonResponse;
import no.qux.demo.hibernate.models.Movie;
import no.qux.demo.hibernate.repositories.MovieRepository;
import no.qux.demo.hibernate.services.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1")
public class MovieController {
    final
    MovieRepository movieRepository;
    final
    MovieService movieService;

    public MovieController(MovieRepository movieRepository, MovieService movieService) {
        this.movieRepository = movieRepository;
        this.movieService = movieService;
    }

    // Create
    @PostMapping("/movies")
    public ResponseEntity<CommonResponse> createMovie(@RequestBody Movie movie) {
        return ResponseEntity
                .ok()
                .body(new CommonResponse(movieRepository.save(movie)));
    }
    // Read
    @GetMapping("/movies")
    public ResponseEntity<CommonResponse> getMovie(){
        return ResponseEntity
                .ok()
                .body(new CommonResponse(movieRepository.findAll()));
    }

    @GetMapping("/movies/{id}")
    public ResponseEntity<CommonResponse> getMovieById(@PathVariable Long id){
        if(movieRepository.findById(id).isPresent()){
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(movieRepository.findById(id).get()));
        }
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }
    @GetMapping("/movies/{id}/characters")
    public ResponseEntity<CommonResponse> getCharactersByMovieId(@PathVariable Long id){
        if(movieRepository.findById(id).isPresent()){
            var movie = movieRepository.findById(id).get();
            var characters = movieService.getCharactersFromMovie(movie);
            var characterLinks = characters.stream().map(character -> "/api/v1/characters/"+ character.getId());
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(characterLinks));
        }
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }

    // Update
    @PutMapping("/movies/{id}")
    public ResponseEntity<CommonResponse> replaceMovie(@PathVariable Long id, @RequestBody Movie c){
        if(movieRepository.findById(id).isPresent()){
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(movieRepository.save(c)));
        }
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }

    // Update
    @PutMapping("/movies/{id}/characters")
    public ResponseEntity<CommonResponse> replaceCharactersInMovie(@PathVariable Long id, @RequestBody Long[] movieIds){
        if(movieRepository.findById(id).isPresent()){
            var movie = movieRepository.findById(id).get();
            movie = movieService.updateCharactersInMovie(movieIds, movie);
            var movieLink = "/api/v1/movies/"+movie.getId();
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(movieLink));
        }
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }
    // Delete
    @DeleteMapping("/movies/{id}")
    public ResponseEntity<CommonResponse> deleteById(@PathVariable Long id){
        var optional = movieRepository.findById(id);
        if(optional.isPresent()){
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(movieService.deleteMovie(id)));
        }
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Object with id not found");
    }

}
