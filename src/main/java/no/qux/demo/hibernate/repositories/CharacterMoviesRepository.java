package no.qux.demo.hibernate.repositories;

import no.qux.demo.hibernate.models.CharactersMovies;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CharacterMoviesRepository extends JpaRepository<CharactersMovies,Long> {
    List<CharactersMovies> getAllByMovieId(Long movieId);
    List<CharactersMovies> getAllByCharacterId(Long characterId);
}
