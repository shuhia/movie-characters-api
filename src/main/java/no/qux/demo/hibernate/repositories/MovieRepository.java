package no.qux.demo.hibernate.repositories;

import no.qux.demo.hibernate.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findMovieByFranchiseId(Long id);
}
