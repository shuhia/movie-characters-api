package no.qux.demo.hibernate.models;

import javax.persistence.*;

@Entity
public class CharactersMovies {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    public long characterId, movieId;

    public CharactersMovies(long characterId, long movieId) {
        this.characterId = characterId;
        this.movieId = movieId;
    }

    public CharactersMovies() {

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
