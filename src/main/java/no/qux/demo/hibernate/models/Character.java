package no.qux.demo.hibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    public String fullName;
    @Column
    public String alias;
    @Column
    public String gender;
    @Column
    public String picture;

    public Long getId() {
        return id;
    }
}
