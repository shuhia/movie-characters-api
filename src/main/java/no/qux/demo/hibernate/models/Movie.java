package no.qux.demo.hibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Stream;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    public String title;
    @Column
    public String genre;
    @Column
    public String releaseYear;
    @Column
    public String director;
    @Column
    public String picture;
    @Column
    public String trailer;
    @Column
    public Long franchiseId;

    public Long getId() {
        return id;
    }
}
