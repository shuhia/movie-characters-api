package no.qux.demo.hibernate.services;

import no.qux.demo.hibernate.models.Character;
import no.qux.demo.hibernate.models.CharactersMovies;
import no.qux.demo.hibernate.models.Franchise;
import no.qux.demo.hibernate.models.Movie;
import no.qux.demo.hibernate.repositories.CharacterMoviesRepository;
import no.qux.demo.hibernate.repositories.CharacterRepository;
import no.qux.demo.hibernate.repositories.FranchiseRepository;
import no.qux.demo.hibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {
    final
    MovieRepository movieRepository;
    final
    FranchiseRepository franchiseRepository;
    final
    CharacterRepository characterRepository;
    final
    CharacterMoviesRepository characterMoviesRepository;
    public MovieService(MovieRepository movieRepository, FranchiseRepository franchiseRepository, CharacterRepository characterRepository, CharacterMoviesRepository characterMoviesRepository) {
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
        this.characterRepository = characterRepository;
        this.characterMoviesRepository = characterMoviesRepository;
    }

    // get all movies from franchise.
    public List<Movie> getMoviesFromFranchise(Franchise franchise){
        Long franchiseId = franchise.getId();
        // Find movies with franchise Id
        var movies =  movieRepository.findMovieByFranchiseId(franchiseId);
        return movies;
    }

    public List<Character> getCharactersFromFranchise(Franchise franchise){
        Long franchiseId = franchise.getId();
        List<Movie> movies = getMoviesFromFranchise(franchise);
        var cIds = movies.stream().map((movie -> characterMoviesRepository.getAllByMovieId(movie.getId()))).flatMap(Collection::stream).map(charactersMovies -> charactersMovies.characterId).toList();
        return characterRepository.findAllById(cIds);
    }

    public List<Character> getCharactersFromMovie(Movie movie){
        Long movieId = movie.getId();
        var list = characterMoviesRepository.getAllByMovieId(movieId).stream().map(log->log.characterId).toList();
        return characterRepository.findAllById(list);
    }

    public List<Movie> updateMoviesInFranchise(Long[] movieIds, Franchise franchise){
        // check if all movies exists
        var franchiseId = franchise.getId();
        var movies = movieRepository.findAllById(Arrays.asList(movieIds));
        // Assign franchise id to all movies
        for (var movie:
                movies) {
            movie.franchiseId = franchiseId;
        }
        return movieRepository.saveAll(movies);
    }

    public Movie updateCharactersInMovie(Long[] characterIds, Movie movie){
        var newCharacters = characterRepository.findAllById(Arrays.asList(characterIds));
        var movieId = movie.getId();
        // Update characters
       var movieCharactersList = characterMoviesRepository.getAllByMovieId(movie.getId());
        characterMoviesRepository.deleteAll(movieCharactersList);
        List<CharactersMovies> newMovieCharactersList = Arrays.stream(characterIds).map(characterId -> new CharactersMovies(characterId,movieId)).toList();
        characterMoviesRepository.saveAll(newMovieCharactersList);

        return movieRepository.save(movie);
    }
    public Long deleteCharacter(Long id){
        // Deletes characters from all related tables
        // Deletes characters from characters movies table
        var rows = characterMoviesRepository.getAllByCharacterId(id);
        characterMoviesRepository.deleteAll(rows);
        // Deletes characters from characters table
        characterRepository.deleteById(id);
        return id;
    }
    public Long deleteMovie(Long id){
        var rows = characterMoviesRepository.getAllByMovieId(id);
        characterMoviesRepository.deleteAll(rows);
        movieRepository.deleteById(id);
        return id;
    }

    public Long deleteFranchise(Long id){
        var rows = movieRepository.findMovieByFranchiseId(id).stream().map(movie -> {
            movie.franchiseId = null;
            return movie;
        }).toList();
        movieRepository.saveAll(rows);
        franchiseRepository.deleteById(id);
        return id;
    }
}
