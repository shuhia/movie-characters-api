# Hibernate Assignment - Movie-Characters-API



[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://hibernate-demo-1.herokuapp.com/swagger-ui/index.html)

Demo deployment of a Spring application to Heroku

## Table of Contents

- [Background](#background)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Demo](#demo)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Hibernate is an Object Relational Model library that provides an easy and standardized way to write Java Spring applications and interact with a variety of databases, and across multiple dialects of SQL. 
This demo demonstrates the use of Hibernate to build a simple REST API that is hosted on Heroku.

The database that we used for the project looks like this:

![img.png](img.png) 

## Built with

- Java/Spring Boot
- Postgres SQL
- Docker


### Deploying Postgres

Postgres is deployed using Docker, using the Docker Compose configuration `docker-compose.yml`. This is setup to accept two environment variables:

- `POSTGRES_DB` &mdash; the name of the postgres database
- `POSTGRES_PASSWORD` &mdash; the only environment variable that is strictly required by the Postgres image.

These are specified in a file that you must create called `.env` (without file extension). This is done by copying `.env.example` and modify values to your preference.

You can then start the Postgres server by running the following command:

```shell
docker-compose up -d postgres
```

### A Primer on URIs

URIs take the following general form:

```
scheme://username:password@target/path/to/your/resource?query=data&foo=bar#fragment
```

Where `target` can be something like a network address of the form: `host[:port]`. For example:

- `foo.example.com`
- `hello.herokuapp.com`
- `localhost:8080`

For this application as an example, the API endpoints for the `MovieController` look something like this:

```
GET http://localhost:8080/api/v1/movies
GET http://localhost:8080/api/v1/movies/:id
GET http://localhost:8080/api/v1/movies/:id/characters
POST http://localhost:8080/api/v1/movies
PUT http://localhost:8080/api/v1/movies/:id
PUT http://localhost:8080/api/v1/movies/:id/characters
DELETE http://localhost:8080/api/v1/movies/:id
```

## Getting Started

### Prerequisites

[Intellij IDE](https://code.visualstudio.com/) or any other editor.

### Installation

1. Clone the repository with the following command

```
    git clone https://gitlab.com/shuhia/movie-characters-api.git
```

2. Open up the project in your editor. When starting, Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.


3. The .env file at the root of the project, remove the .example extension from the file name as explained in [deploying postgres](#deploying-postgres) section.


4. Run docker compose to build the container for your database

```
    docker-compose up -d postgres
```

5. Start your Spring Boot Application.

## Usage

To test out the API for this application locally, head to the Swagger/OpenApi interface and interact with the API endpoints
- [https://localhost:8080/swagger-ui/index.html](https://localhost:8080/swagger-ui/index.html)


## Demo

To test out the deployed version of the API, head to the Swagger/OpenApi interface and interact with the API endpoints
- [https://hibernate-demo-1.herokuapp.com/swagger-ui/index.html](https://hibernate-demo-1.herokuapp.com/swagger-ui/index.html)


## Maintainers

- [Alex On (@shuhia)](https://gitlab.com/shuhia)
- [Sebastian Börjesson (@sebastian.borjesson)](https://gitlab.com/sebastian.borjesson)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

